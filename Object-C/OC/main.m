//
//  main.m
//  OC
//
//  Created by 何海涛 on 14/12/2.
//  Copyright (c) 2014年 ltbl. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Login.h"
#import "Fraction.h"
#import "ClassA.h"
#import "ClassB.h"
#import "Exception.h"

void except(){
    Exception* ex = [[Exception alloc]init];
    @throw ex;
}

int main(int argc, const char * argv[])
{
    
    //NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
    //@autoreleasepool
    {
        
        // insert code here...
        NSLog(@"--OC hello 20140823");
        NSLog(@"Hello, Test!");
        
        NSLog(@"--OC fraction 20140824");
        Fraction* frac1 = [[Fraction alloc]init];
        [frac1 setNumber:100];
        [frac1 setIds:101];
        printf("frac1 print:");
        [frac1 print];
//        [frac1 release];
        
        Fraction* frac2 = [[Fraction alloc]initWithNumberator:200 andWith:400];
        printf("frac2 print:");
        [frac2 print];
//        [frac2 release];
        
        NSLog(@"--OC login 20140825");
        test2 *t2 = [[test2 alloc] init];
        [t2 setUsername:@"hehaitao"];
        [t2 setPassword:@"88888888"];
        [t2 loginUser:@"hehaitao" andWith:@"8888888"];
//        [t2 release];
        
        NSLog(@"--OC class 20140826");
        id var;
        ClassA* t = [[ClassA alloc] init];
        var = t;
        [var print];
        [(ClassA*)var print];
        
        NSLog(@"--OC exception 20140827");
        @try {
            except();
            NSLog(@"try");
        }
        @catch (Exception* ex) {
//            [ex release];
            NSLog(@"release");
        }
        @finally {
            NSLog(@"finally");
        }
        
        NSLog(@"--OC datatype 20140922");
        int i = 100;
        float f = 45.334;
        double d = 45.334;
        char c = 'W';
        NSLog(@"%i, %f, %e, %g, %c", i, f, d, d, c);
        
    }
    
    //[pool release];
    
    /*
     *注释如下操作
     */
    
    
    return 0;
}

