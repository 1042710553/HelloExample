//
//  test3.h
//  OC_01
//
//  Created by APPLE on 14-8-18.
//  Copyright (c) 2014年 ht. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Rectangle : NSObject
{
    int width;
    int height;
}

-(Rectangle*) initWithWidth: (int) w andWithHeight: (int) h;
-(void) setWidth:(int) w;
-(void) setHeight:(int) h;
-(void) setWidth:(int)w height:(int) h;
-(int) width;
-(int) heigth;
-(void) print;

@end
