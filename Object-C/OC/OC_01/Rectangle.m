//
//  test3.m
//  OC_01
//
//  Created by APPLE on 14-8-18.
//  Copyright (c) 2014年 ht. All rights reserved.
//

#import "Rectangle.h"

@implementation Rectangle

-(Rectangle*) initWithWidth:(int)w andWithHeight:(int)h
{
    self = [super init];
    if(self){
        [self setWidth:w height:h];
    }
    return self;
}

-(void) setWidth:(int)w height:(int)h{
    width = w;
    height = h;
}

-(void) setWidth:(int)w{
    width = w;
}

-(void) setHeight:(int)h{
    height = h;
}

-(void) print{
    NSLog(@"width=%i, height=%i", width, height);
}

-(int) width{
    return width;
}

-(int) heigth{
    return  height;
}

@end
