//
//  test2.h
//  OC_01
//
//  Created by APPLE on 14-8-15.
//  Copyright (c) 2014年 ht. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface test2 : NSObject
{
    NSString* username;
    NSString* password;
}

@property(nonatomic, copy) NSString* username;
@property(nonatomic, copy) NSString* password;

-(BOOL) loginUser:(NSString *)_username andWith:(NSString*)_password;
@end
