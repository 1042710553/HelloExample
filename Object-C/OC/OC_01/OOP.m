//
//  OOP.m
//  OC
//
//  Created by hthe on 14/12/2.
//  Copyright (c) 2014 ltbl. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OOP.h"

@implementation Circle

- (void) setFillColor: (ShapeColor) _fillColor
{
    fillColor = _fillColor;
} //setFillColor

- (void) setBounds: (ShapeRect) _bounds
{
    bounds = _bounds;
} //setBounds

- (void) draw
{
    NSLog(@"drawing a circle at (%d %d %d %d) in %@", bounds.x, bounds.y, bounds.width, bounds.height, colorName(fillColor));
} //draw

NSString *colorName(ShapeColor colorName)
{
    switch(colorName) {
        case kRedColor:
            return @"red";
            break;
        case kGreenColor:
            return @"green";
            break;
        case kBlueColor:
            return @"blue";
            break;
    }
    
    return @"no clue";
} //colorName

@end