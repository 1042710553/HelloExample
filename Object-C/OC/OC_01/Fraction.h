//
//  test1.h
//  test1
//
//  Created by APPLE on 14-8-13.
//  Copyright (c) 2014年 ht. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Fraction : NSObject
{
	int number;
	int ids;
}

//@property(nonatomic) int number;
//@property(nonatomic) int ids;

-(Fraction*) initWithNumberator:(int)n andWith:(int)d;
-(void) setNumberator:(int)n andWith:(int) d;
-(void) print;

-(void) setNumber:(int)n;
-(void) setIds:(int)i;

@end
