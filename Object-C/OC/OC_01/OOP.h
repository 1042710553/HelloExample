//
//  OOP.h
//  OC
//
//  Created by 何海涛 on 14/12/2.
//  Copyright (c) 2014年 ltbl. All rights reserved.
//

#ifndef OC_OOP_h
#define OC_OOP_h
typedef enum {
    kCircle,
    kRectangle,
    kEgg
} ShapeType; //ShapeType

typedef enum {
    kRedColor,
    kGreenColor,
    kBlueColor
} ShapeColor; //ShapeColor

typedef struct {
    int x, y, width, height;
} ShapeRect; //ShapeRect

typedef struct {
    ShapeType type;
    ShapeColor fillColor;
    ShapeRect bounds;
} Shape; //Shape

@interface Circle : NSObject
{
@private
    ShapeColor fillColor;
    ShapeRect bounds;
}

- (void) setFillColor: (ShapeColor) fillColor;
- (void) setBounds: (ShapeRect) bounds;
- (void) draw;

@end



#endif
