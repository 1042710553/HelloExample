//
//  test1.m
//  test1
//
//  Created by APPLE on 14-8-13.
//  Copyright (c) 2014年 ht. All rights reserved.
//

#import "Fraction.h"

@implementation Fraction

//@synthesize number;
//@synthesize ids;

-(Fraction*) initWithNumberator:(int)n andWith:(int)d
{
    self = [super init];
    if(self){
        [self setNumberator:n andWith: d];
    }
    
    return self;
}

-(void) setNumberator:(int)n andWith:(int)d
{
    number = n;
    ids = d;
}

-(void) print{
	NSLog(@"x=%i,y=%i", number, ids);
}

-(void) setNumber:(int)n{
	number = n;
}

-(void) setIds:(int) i{
	ids = i;
}
@end
