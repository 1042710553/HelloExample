// Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"
var possibleNumber = "1"
var convertedNumber = possibleNumber.toInt()
if convertedNumber != nil {
    println("\(possibleNumber) 转换为数字后等于 \(convertedNumber!)")
}
else{
    println("\(possibleNumber) 不能转换为数字")
}

class Country {
    let name: String
    let capitalCity: City!
    init(name: String, capitalName:String){
        self.name = name
        self.capitalCity = City(name: capitalName, country: self)
    }
}

class City {
    let name: String
    unowned let country: Country
    init(name: String, country: Country) {
        self.name = name
        self.country = country
    }
}

var country = Country(name: "china", capitalName: "beijing")
println(country.capitalCity.name)

convertedNumber = nil
let optionString : String? = "let go"
let possibleString: String? = "an optional string"

let age = 0
assert(age >= 0, "not < 0")

var string = ""
if string.isEmpty {
    string = "hello,world!"
}

string = "特殊字符均用斜线符号进行转义：\0=null \\ \t=Tab \n=换行 \r=回车 \" \'"
println(string)

string = "你好心世界!"
for character in string {
    if character == "心" {
        println("从\(countElements(string))个字符中找到心了")
        break
    }
}

string = "prefix containString suffix"
println(string.hasPrefix("pre"))
println(string.hasPrefix("fix"))

let normal = "Have a test"
println("大写：\(normal.uppercaseString)小写：\(normal.lowercaseString)")

for index in 0...3 {
    print(index) //0123
}

//var dogPictureUrl = NSURL(string: "http://i.imgur.com/kLVE6E9.jpg")

fun sayHello(personName: String) ->String
{
    return "Hello \(personName) !"
}
println(sayHello("Parishe"));

fun toString(s1: String, s2: String, joiner: String) ->String
{
    return s1 + joiner + s2;
}






