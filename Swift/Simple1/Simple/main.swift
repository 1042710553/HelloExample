//
//  main.swift
//  Simple
//
//  Created by CarolWang on 14/12/10.
//  Copyright (c) 2014年 com.jikexueyuan. All rights reserved.
//

import Foundation

println("Hello, World!")
//1.基本数据类型 Int 整型 ， Double 和 Float 浮点型， Bool 布尔值，String 文本型数据
//2.常量 let， 变量 var
let con = 100;
var avi = 30;
avi = 40

//3.一行中声明多个变量或者常量，用逗号隔开
var a = 3, b = 4, c = 5

//4.类型标注：如果声明的同时赋了初始值，并不需要类型标注
var Who: String
Who = "xiaoming"

//5.变量和常量的命名：不能包含数学符号，箭头，连线与制表符，不能以数字开头
let 你好 = "nihao"
println(你好)

var 😊 = "笑"
println(😊)

var 眼睛 = "👀"
println(眼睛)

//6.字符串插值
var apples = 10
var oranges = 4
println("I have \(apples + oranges) fruits")

//7.注释 多行注释可以嵌套
/*

/*
第一层注释
第二层注释
*/

*/
