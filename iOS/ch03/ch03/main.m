//
//  main.m
//  ch03
//
//  Created by 何海涛 on 15/3/18.
//  Copyright (c) 2015年 hello. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
