//
//  ViewController.m
//  ch03
//
//  Created by 何海涛 on 15/3/18.
//  Copyright (c) 2015年 hello. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)onClick:(id)sender {
    self.Label1.text = @"HelloWorld";
}

- (IBAction)switchValueChanged:(id)sender {
    UISwitch *witch = (UISwitch *)sender;
    BOOL selected = witch.isOn;
    [self.leftSwitch setOn:selected animated:YES];
    [self.rightSwitch setOn:selected animated:YES];
}

- (IBAction)sliderValueChanged:(id)sender {
    UISlider *slider = (UISlider *)sender;
    int progress = (int)(slider.value + 0.5f);
    NSString *newText = [[NSString alloc] initWithFormat:@"%d", progress];
    self.sliderValue.text = newText;
}

- (IBAction)touchDownChanged:(id)sender {
    if (self.leftSwitch.hidden  == YES) {
        self.rightSwitch.hidden = NO;
        self.leftSwitch.hidden  = NO;
    }else{
        self.leftSwitch.hidden  = YES;
        self.rightSwitch.hidden = YES;
    }
}



- (void) viewWillAppear:(BOOL)animated {
    
    //注册键盘出现通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (keyboardDidShow:)
                                                 name: UIKeyboardDidShowNotification object:nil];
    //注册键盘隐藏通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (keyboardDidHide:)
                                                 name: UIKeyboardDidHideNotification object:nil];
    [super viewWillAppear:animated];
}


-(void) viewWillDisappear:(BOOL)animated {
    //解除键盘出现通知
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name: UIKeyboardDidShowNotification object:nil];
    //解除键盘隐藏通知
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name: UIKeyboardDidHideNotification object:nil];
    
    [super viewWillDisappear:animated];
}

-(void) keyboardDidShow: (NSNotification *)notif {
    NSLog(@"键盘打开");
}

-(void) keyboardDidHide: (NSNotification *)notif {
    NSLog(@"键盘关闭");
}

//通过委托来实现放弃第一响应者
#pragma mark - UITextField Delegate Method
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


//通过委托来实现放弃第一响应者
#pragma mark - UITextView Delegate  Method
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

@end
