//
//  ViewController.h
//  ch03
//
//  Created by 何海涛 on 15/3/18.
//  Copyright (c) 2015年 hello. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITextFieldDelegate, UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *Label1;
- (IBAction)onClick:(id)sender;


@property (weak, nonatomic) IBOutlet UISwitch *leftSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *rightSwitch;
@property (weak, nonatomic) IBOutlet UILabel *sliderValue;

- (IBAction)switchValueChanged:(id)sender;
- (IBAction)sliderValueChanged:(id)sender;
- (IBAction)touchDownChanged:(id)sender;

@end

