//
//  AppDelegate.h
//  ch03
//
//  Created by 何海涛 on 15/3/18.
//  Copyright (c) 2015年 hello. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

