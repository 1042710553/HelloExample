//
//  ViewController.h
//  ch3_5
//
//  Created by 何海涛 on 15/3/19.
//  Copyright (c) 2015年 hello. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webview;
- (IBAction)testloadHTMLString:(id)sender;
- (IBAction)testLoadData:(id)sender;
- (IBAction)testloadRequest:(id)sender;

@end

